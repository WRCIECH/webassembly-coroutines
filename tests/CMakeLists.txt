include(GoogleTest)

set(sources
   test_project_feature.cc
)

add_executable(SummatorTests ${sources} )

target_link_libraries(SummatorTests PRIVATE gtest gtest_main)
gtest_discover_tests(SummatorTests
    DISCOVERY_MODE PRE_TEST
    TEST_FILTER *
    TEST_LIST SummatorTestList
)