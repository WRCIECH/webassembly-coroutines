#include <emscripten/bind.h>
#include <coroutine>

using namespace emscripten;

class DocumentObjectModel {
public:
    virtual ~DocumentObjectModel() = default;
    virtual void appendText(std::string const& text) = 0;

    void setCoroutineHandle(std::coroutine_handle<> h) {
        h_ = h;
    }
    void resetCoroutine() {
        h_ = std::nullopt;
    }
    void textClicked() {
        if(h_.has_value()) {
            h_->resume();
        }
    }
private:
    std::optional<std::coroutine_handle<>> h_;
};


auto waitUntilUserClicksText(DocumentObjectModel* dom)
{
    struct awaitable
    {
        DocumentObjectModel* dom_;
        bool await_ready() { return false; }
        void await_suspend(std::coroutine_handle<> h) {
            dom_->setCoroutineHandle(h);
        }
        void await_resume() {
            dom_->resetCoroutine();
        }
    };
    return awaitable{dom};
}

struct JavascriptTask {
    struct promise_type {
        JavascriptTask get_return_object() { return {}; }
        std::suspend_never initial_suspend() { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }
        void return_void() {}
        void unhandled_exception() {}
    };
};

JavascriptTask interactWithBrowser(DocumentObjectModel* dom) {
    dom->appendText("Hello! Click This Text!");
    co_await waitUntilUserClicksText(dom);

    dom->appendText("Now Click THIS Text!");
    co_await waitUntilUserClicksText(dom);

    dom->appendText("Now you need to only click this text to finish!");
    co_await waitUntilUserClicksText(dom);

    dom->appendText("Function Ended!");
    co_return;
}

void interactWithBrowserBare(DocumentObjectModel* dom) {
    interactWithBrowser(dom);
}

class DocumentObjectModelWrapper : public wrapper<DocumentObjectModel> {
public:
    EMSCRIPTEN_WRAPPER(DocumentObjectModelWrapper);

    void appendText(std::string const& text) override {
        call<void>("appendText", text);
    }
};

EMSCRIPTEN_BINDINGS(DocumentObjectModel) {
    class_<DocumentObjectModel>("DocumentObjectModel")
            .smart_ptr<std::shared_ptr<DocumentObjectModel>>("DocumentObjectModel")
            .function("appendText", &DocumentObjectModel::appendText, pure_virtual())
            .function("textClicked", &DocumentObjectModel::textClicked)
            .allow_subclass<DocumentObjectModelWrapper>("DocumentObjectModelWrapper");

    function("interactWithBrowser", &interactWithBrowserBare, allow_raw_pointers());
}

int main() { }
