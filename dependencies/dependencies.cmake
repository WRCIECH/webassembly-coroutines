include(FetchContent)

Set(FETCHCONTENT_QUIET FALSE)

FetchContent_Declare(googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG e2239ee6043f73722e7aa812a459f54a28552929 # release-1.11.0
    GIT_PROGRESS TRUE
    GIT_SHALLOW ON
)

# Get rid of MACOS RPATH warning (https://stackoverflow.com/a/31567991/14599759)
set(CMAKE_MACOSX_RPATH 1)
set(INSTALL_GTEST OFF)
fetchContent_MakeAvailable(googletest)
