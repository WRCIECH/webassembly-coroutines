# C++20 Coroutines

Coroutines is a new feature of C++ added in [C++20 standard](https://en.cppreference.com/w/cpp/language/coroutines). It provides low level functionalities to write asynchronous code that can be read just like
plain synchronous code. 

## C++20 Coroutine (very) basics
See [picture](http://www.vishalchovatiya.com/cpp20-coroutine-under-the-hood/) to quick understanding what coroutines are about

Coroutines can be used for:
- Asynchronous operations (usage with `co_await`)
- Lazy computation (usage with `co_yield`)

### C++20 Coroutine Concepts
#### Coroutine Promise Object

Determines coroutine "global" properties and behaviour. Object returned by coroutine needs to provide corresponding promise.
It's either `Object::promise_type` or via using `coroutine_traits`.
- It can decide the coroutine will be suspended immediately after starting it
- It can decide the coroutine will be suspended just before the return
- It decides how coroutine handle is "handled" to external world
```cpp
struct promise_type {
    JavascriptTask get_return_object() { return {}; }
    std::suspend_never initial_suspend() { return {}; }
    std::suspend_never final_suspend() noexcept { return {}; }
    void return_void() {}
    void unhandled_exception() {}
};
```

#### Coroutine Handle
It's a handle to coroutine that is mainly used for resuming the coroutine. There is an abstraction in C++ for it: 
[std::coroutine_handle<>](https://en.cppreference.com/w/cpp/coroutine/coroutine_handle)
#### Awaitable & Awaiter types
These two concepts (one type can be both Awaiter and Awaitlable) defines what exactly happens when calling:
```
co_await awaitable_type;
```
that is:
- If suspend has to happen (`await_ready`)
- What to do when suspension happens (`await_suspend`) - this is the place where the coroutine handle should be passed "somewhere"
- What should happen when coroutine is resumed (coroutine is resumed when external context calls `coroutine_handle.resume()`)

API:
```cpp
struct awaitable
{
    DocumentObjectModel* dom_;
    bool await_ready() { return false; }
    void await_suspend(std::coroutine_handle<> h) {
        dom_->setCoroutineHandle(h);
    }
    void await_resume() {
        dom_->resetCoroutine();
    }
};
```

## Recommended Resources

- [Asymmetric Transfer](https://lewissbaker.github.io/) posts written by Lewis Baker provides very detailed information about implementing coroutines in C++
- I recommend also reading [this post](https://www.scs.stanford.edu/~dm/blog/c++-coroutines.html) and [Raymond Chen articles](https://devblogs.microsoft.com/oldnewthing/20191209-00/?p=103195)

## Running sample

### Simple Example in Emscripten
1. `git checkout main`
2. `toolchainFile` in CMakePresets.json needs to be filled appropriately with path to emsdk.
3. Configure & compile to webassembly
```
cmake --preset=webassembly-release
cmake --build --preset=webassembly-release
```
(there is also debug equivalents)
4. Run sample
```
cd web
npm run dev
```

### Downloading stuff from server using Emscripten Fetch API Example
1. `git checkout coroutines-used-for-fetching-data-from-server`
2. `toolchainFile` in CMakePresets.json needs to be filled appropriately with path to emsdk.
3. Configure & compile to webassembly
```
cmake --preset=webassembly-release
cmake --build --preset=webassembly-release
```
(there is also debug equivalents)

4. Run fake server
```
cd web
npm run server
```
5. Run sample
```
cd web
npm run dev
```
