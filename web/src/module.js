import createMyModule from '@/../CoroutinesForWeb.js'

let Module;
let DocumentObjectModel;
let dom;

createMyModule().then(MyModule => {
    Module = MyModule;
    DocumentObjectModel = Module.DocumentObjectModel.extend("DocumentObjectModel", {
        appendText: function(text) {
            postMessage({
                command: 'appendText',
                value: text
            })
        },
    });
    dom = new DocumentObjectModel();

    postMessage({
        command: 'moduleInitialized'
    })

    // TODO: Call C++ coroutine to start interaction
    Module.interactWithBrowser(dom);
});

addEventListener('message', function(e) {
    const command = e.data.command;
    if (command) {
        if(command === 'textClicked') {
            dom.textClicked();
        }
    }
});